import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import UserListItem from '../components/UserListItem'
import renderer from 'react-test-renderer';

test("UserListItem renders correctly", async () => {

  const tree = renderer
    .create(<UserListItem user={{ name: "name1", last_name: "", id: "1", email: "testing@email.com" }} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
})