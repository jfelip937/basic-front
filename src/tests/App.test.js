import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import App from '../pages/SignInPage'
import renderer from 'react-test-renderer';
import axiosMock from 'axios'

jest.mock('axios')

test("App renders SigIn when user not authenticated", async () => {

  axiosMock.get.mockResolvedValueOnce({
    data: [
      { name: "name1", last_name: "", id: "1", email: "testing@email.com" },
      { name: "name2", last_name: "", id: "2", email: "testing2@email.com" }
    ]
  })

  const tree = renderer
    .create(<App ></App>)
    .toJSON();
  expect(tree).toMatchSnapshot();
})
