import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import UserList from '../components/UserList'
import renderer from 'react-test-renderer';
import axiosMock from 'axios'

jest.mock('axios')

test("UserList renders correctly", async () => {

  axiosMock.get.mockResolvedValueOnce({
    data: [
      { name: "name1", last_name: "", id: "1", email: "testing@email.com" },
      { name: "name2", last_name: "", id: "2", email: "testing2@email.com" }
    ]
  })

  const tree = renderer
    .create(<UserList snackbar={jest.fn()} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
})