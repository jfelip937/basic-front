import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import AppBar from '../components/AppBar'
import renderer from 'react-test-renderer';

test("AppBar renders correctly", async () => {

  const tree = renderer
    .create(<AppBar snackbar={jest.fn()} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
})