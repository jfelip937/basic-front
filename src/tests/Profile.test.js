import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import Profile from '../pages/Profile'
import renderer from 'react-test-renderer';
import axiosMock from 'axios'

jest.mock('axios')

test("Profile renders correctly", async () => {

  axiosMock.get.mockResolvedValueOnce({
    data: { user: {name: "name", last_name: "", id: "1", email: "testing@email.com"} },
  })

  const tree = renderer
    .create(<Profile match={{params: {id: 1}}}></Profile>)
    .toJSON();
  expect(tree).toMatchSnapshot();
})
