import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import SignUpPage from '../pages/SignUpPage'
import renderer from 'react-test-renderer';


jest.mock('axios')

test("SingUpPage renders correctly", async () => {
  const tree = renderer
    .create(<SignUpPage ></SignUpPage>)
    .toJSON();
  expect(tree).toMatchSnapshot();
})
