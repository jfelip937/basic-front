import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import SignInPage from '../pages/SignInPage'
import renderer from 'react-test-renderer';

test('SignInPage renders correctly', () => {
  const tree = renderer
    .create(<SignInPage ></SignInPage>)
    .toJSON();
  expect(tree).toMatchSnapshot();
})