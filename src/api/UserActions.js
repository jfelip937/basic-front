import history from "../components/history"
import axios from "axios"

const api_path = 'http://localhost:3001/api/v1'

export function SignOut(snackbar) {
  axios.delete(api_path + '/users/sign_out', { headers: { Authorization: localStorage.getItem("jwt") } })
    .then(response => {
      localStorage.removeItem("jwt")
      localStorage.removeItem("user")
      history.push("/signin")
    })
    .catch(error => {
      snackbar("Something went wrong.")
    })
}

export function SignIn(snackbar, data) {
  axios.post(api_path + '/users/sign_in', data)
    .then(response => {
      localStorage.setItem("jwt", response.headers["authorization"])
      localStorage.setItem("user", JSON.stringify(response.data))
      history.push("/")
    })
    .catch(error => {
      let message = "Something went wrong."
      if (error.response !== null && error.response !== 'undefined') message = error.response.data
      snackbar(message)
    })
}

export function SignUp(snackbar, data) {
  axios.post(api_path + '/users/', data)
    .then(response => {
      history.push("/signin")
    })
    .catch(error => {
      snackbar("Something went wrong.")
    })
}

export function GetUser(snackbar, id) {
  return (
    axios.get(api_path + '/users/' + id, { headers: { Authorization: localStorage.getItem("jwt") } })
      .then(response => {
        let user = response.data
        if (user.last_name === null) user.last_name = ''
        return (
          {
            firstName: user.name,
            lastName: user.last_name,
            email: user.email
          }
        )
      })
      .catch(error => {
        snackbar("Something went wrong.")
      })
  )
}

export function UpdateUser(snackbar, id, data) {
  axios.put(api_path + '/users/' + id, data, { headers: { Authorization: localStorage.getItem("jwt") } })
    .then(response => {
      snackbar("User succesfully updated.")
    })
    .catch(error => {
      snackbar("Something went wrong.")
    })
}

export function DestroyUser(snackbar, id) {
  axios.delete(api_path + '/users/' + id, { headers: { Authorization: localStorage.getItem("jwt") } })
    .then(response => {
      localStorage.removeItem("jwt")
      localStorage.removeItem("user")
      history.push("signin")
    })
    .catch(error => {
      snackbar("Something went wrong.")
    })
}