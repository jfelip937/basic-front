import React from "react"
import { Router, Route, Redirect } from "react-router-dom"
import App from "./App"
import SignInPage from "./SignInPage"
import history from "../components/history"
import SignUpPage from "./SignUpPage"
import Profile from "./Profile"

function AppRouter() {
  return (
    <Router history={history}>
      <Route exact path="/" render={() => (
        localStorage.getItem("jwt") ? (
          <App />
        ) : (
            <Redirect to="/signin" />
          )
      )} />

      <Route exact path="/signin" render={() => (
        localStorage.getItem("jwt") ? (
          <Redirect to="/" />
        ) : (
            <SignInPage />
          )
      )} />

      <Route exact path="/signup" render={() => (
        localStorage.getItem("jwt") ? (
          <Redirect to="/" />
        ) : (
            <SignUpPage />
          )
      )} />

      <Route exact path="/users/:id" render={(props) => (
        localStorage.getItem("jwt") ? (
          <Profile {...props} />
        ) : (
            <Redirect to="/signin" />
          )
      )} />

    </Router>
  )
}

export default AppRouter