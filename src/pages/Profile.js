import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from '../components/AppBar'
import Snackbar from '@material-ui/core/Snackbar'
import { GetUser } from '../api/UserActions'
import { UpdateUser } from '../api/UserActions'
import { DestroyUser } from '../api/UserActions'

const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class Profile extends React.Component {

  constructor(props) {
    super(props);

    let isCurrentUser = false
    try{
      if (parseInt(props.match.params.id) === JSON.parse(localStorage.getItem("user"))["id"]) isCurrentUser = true
    }
    catch(e){}

    this.state = {
      id: props.match.params.id,
      firstName: "",
      lastName: "",
      email: "",
      isCurrentUser: isCurrentUser
    }
    this.showSnack = this.showSnack.bind(this)
  }

  componentDidMount() {
    GetUser(this.showSnack, this.state.id).then(response => {
      this.setState(response)
    })
  }

  change(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  submit(e) {
    e.preventDefault();
    let data = { "user": { "email": this.state.email, "name": this.state.firstName, "last_name": this.state.lastName } }
    UpdateUser(this.showSnack, this.state.id, data)
  }

  destroy(e) {
    DestroyUser(this.showSnack, this.state.id)
  }

  showSnack(message) {
    this.setState({ openSnack: true, messageSnack: message })
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar snackbar={this.showSnack} />
        <Container maxWidth="xs" className={classes.paper}>
          <Typography component="h1" variant="h5">
            Profile
          </Typography>
          <form className={classes.form} onSubmit={e => this.submit(e)}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required={this.state.isCurrentUser}
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  onChange={e => this.change(e)}
                  value={this.state.firstName}
                  disabled={!this.state.isCurrentUser}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                  onChange={e => this.change(e)}
                  value={this.state.lastName}
                  disabled={!this.state.isCurrentUser}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required={this.state.isCurrentUser}
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  onChange={e => this.change(e)}
                  value={this.state.email}
                  disabled={!this.state.isCurrentUser}
                />
              </Grid>
            </Grid>
            {
              this.state.isCurrentUser &&
              <div>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Update
            </Button>
                <Button
                  type="button"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  className={classes.submit}
                  onClick={e => this.destroy(e)}
                >
                  Destroy User
            </Button>
              </div>
            }
          </form>
        </Container>
        <Snackbar open={this.state.openSnack} autoHideDuration={3000} message={this.state.messageSnack} onClose={() => this.setState({ openSnack: false })} />
      </React.Fragment>
    )
  }
}

export default withStyles(useStyles)(Profile);