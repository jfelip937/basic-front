import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';
import AppBar from "../components/AppBar"
import history from "../components/history"
import Typography from '@material-ui/core/Typography';
import UserList from '../components/UserList'
import Grid from '@material-ui/core/Grid'
import Snackbar from '@material-ui/core/Snackbar'

class App extends React.Component {

  constructor(props) {
    super(props)

    let name = (JSON.parse(localStorage.getItem("user"))) ? JSON.parse(localStorage.getItem("user"))["name"] : ""

    this.state = { 
      openSnack: false,
      messageSnack: "",
      user: name
    }
    this.showSnack = this.showSnack.bind(this)
  }

  showSnack(message) {
    this.setState({ openSnack: true, messageSnack: message })
  }

  render() {
    if (localStorage.getItem("user") === null || localStorage.getItem("user") === "undefined"
      || localStorage.getItem("jwt") === null || localStorage.getItem("jwt") === "undefined") {
      localStorage.removeItem("jwt")
      localStorage.removeItem("user")
      history.push("/signin")
    }

    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar snackbar={this.showSnack} />
        <Typography variant="h2" align="center">Welcome {this.state.user}</Typography>
        <Grid container justify="center">
          <UserList snackbar={this.showSnack} />
        </Grid>
        <Snackbar open={this.state.openSnack} autoHideDuration={3000} message={this.state.messageSnack} onClose={() => this.setState({ openSnack: false })} />
      </React.Fragment>
    )
  }
}

export default App;