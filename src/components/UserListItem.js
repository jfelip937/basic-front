import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import history from "./history"

class UserListItem extends React.Component {

  constructor(props) {
    super(props)
    let user = props.user
    if (user.last_name === null) user.last_name = ''
    this.state = {
      user: user
    }
  }

  showProfile(e) {
    let path = "/users/" + this.state.user.id
    history.push(path)
  }

  render() {
    return (
      <ListItem button={true} alignItems="flex-start" onClick={(e) => this.showProfile(e)}>
        <ListItemText
          primary={this.state.user.name + " " + this.state.user.last_name}
          secondary={
            <React.Fragment>
              {this.state.user.email}
            </React.Fragment>
          }
        />
      </ListItem>
    )
  }
}

export default UserListItem