import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import App from '../pages/App'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { SignOut } from '../api/UserActions'
import Link from '@material-ui/core/Link'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function ButtonAppBar(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography onClick={App} variant="h6" className={classes.title}>
            <Link href="/" color="inherit" >Home</Link>
          </Typography>
          <Button onClick={() => SignOut(props.snackbar)} color="inherit">Signout</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}