import React from 'react';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import axios from 'axios'
import UserListItem from './UserListItem';
import { withStyles } from '@material-ui/core/styles';

const useStyles = theme => ({
  root: {
    width: '50%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
});

class UserList extends React.Component {

  constructor(props) {
    super()
    this.state = { users: [], snackbar: props.snackbar }
  }

  componentDidMount() {
    axios.get('http://localhost:3001/api/v1/users/', { headers: { Authorization: localStorage.getItem("jwt") } })
      .then(response => {
        this.setState({ users: response.data })
      })
      .catch(error => {
        let message = "Something went wrong."
        this.state.snackbar(message)
      })
  }

  render() {
    const { classes } = this.props;
    return (
      <List className={classes.root} >
        {
          this.state.users.map((user, index) => {
            const notLast = index < this.state.users.length - 1
            return (
              <div key={user.id}>
                <UserListItem user={user} />
                {notLast ? (<Divider component="li" />) : null}
              </div>
            )
          })
        }
      </List>
    );
  }
}

export default withStyles(useStyles)(UserList)